---
home: true
heroImage: /TMS-Logo.png
actionText: C'est parti →
actionLink: tms/
---

Bienvenue au Syndicat Mayhem, nous nous unissons en tant que frères d'armes et causons le chaos aux groupes importants et militaires à travers tout Robloxia. En utilisant ds armes de hautes technologies et de technologies aliens, nous plions ce qui s'opposent à notre volonté.

Nous célébrons principalement la destruction en tant que groupe de raid du Pinewood Computer Core, où nous attaquons le noyau et ne laissant pas la PBST nous gêner.

::: danger ATTENTION! 
Veuillez lire attentivement toutes les règles. Toute modification des règles sera clarifiée. Les règles ont été catégorisées en fonction du groupe auxquelles elles s'appliquent. Toutefois, certaines règles peuvent s'appliquer à d'autres groupes que le TMS, auquel cas elles seront précisées. Les sanctions pour avoir enfreint une règle dépendent de celle-ci et de sa sévérité. Les sanctions peuvent aller d'un simple avertissement à une rétrogradation ou encore à un bannissement du TMS. 
:::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a></center>
