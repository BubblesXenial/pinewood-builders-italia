---
sidebar: auto
sidebarDepth: 5
---

# Manuel du TMS

## Rappels généraux
---
* Être respectueux avec les autres membres, spécialement avec ceux qui vous dépassent, nous jouons tous pour s'amuser.
* Ne pas essayer d’échapper aux sanctions, cela ne la rendra que l'empirer.
* Nous ne sommes pas en guerre avec les autres groupes, les hostilités envers eux ne sont pas bienvenues.
* **Tuer au spawn et tuer aléatoirement en masse** est interdit pour tout le monde, tout le temps, et sera sévèrement puni. – **Le spawn s'étend depuis hall de spawn, à travers la boutique entière du spawn, aux escaliers et à l'ascenseur. Le spawn se termine à la ligne jaune en bas de l'ascenseur.** - La zone de l'ascenseur du PBRF attachée au hall du spawn et la Salle Easy Escape sont aussi considérées comme une zone de spawn ! - Camper en bas de l'ascenseur est mal vu, essayez d'attaquer ailleurs !
* Merci de toujours suivre les règles de la communauté de roblox, ceci est juste un jeu !
* Attaquer les autres membres du TMS en service est mal vu et peut mener à une sanction.
---

## Règles pendant les raids
Ces règles sont pour tout le monde ayant leur rang réglé sur le Syndicat Mayhem (le titre "**Syndicate**" en rouge)

### Uniformes
*Lorsque votre rang est réglé sur TMS, vous êtes considérés **en service** pour le TMS et êtes requis de porter l'uniforme officiel du TMS !* Vous pouvez trouvez le **refuge du TMS** au Pinewood Computer Core près de la plateforme des trains Cargos.

Le refuge du TMS est situé près du spawn du train Cargo au Pinewood Computer Core

Vous n'êtes **pas** autorisé à porter un uniforme de la PBST en service pour le TMS, ceci est sévèrement puni si vous êtes attrapé.

::: tip Combinaisons de la PET/du noyau 
Vous êtes autorisé à porter les combinaisons de la PET ou du noyau en service pour le TMS. Un uniforme n'est pas requis si vous portez ces combinaisons. 
:::


Les instructeurs + sont autorisés à porter des uniformes modifiés/personnalisés.

Si vous avez [les commandes de donateurs](https://www.roblox.com/game-pass/3497976/Donor-Commands), vous pouvez vous-même vous donnez un uniforme via les commandes `!shirt` et `!pants` ID des textures d'uniformes Chemises : [`!shirt 4428565863`](https://www.roblox.com/catalog/4428565917/OwO) Pantalons : [`!pants 4428568303`](https://www.roblox.com/catalog/4428565917/Funny-Easteregg-This-Is-Not-Real-Link-name)

Encore mieux, vous pouvez combiner ces deux-là en un alias, exécutez cette commande dans le chat `!addalias !tmsuniform !shirt 4428565863 & !pants 4428568303`  
Ensuite, à chaque fois que vous exécuter `!tmsuniform` dans le chat ou la console, vous mettrez l'uniforme du TMS !

[**Vous devez avoir les commandes de donateurs pour faire ce qui précède**](https://www.roblox.com/game-pass/3497976/Donor-Commands)

### Armes
Les membres du syndicat reçoivent des armes au Pinewood Computer Core pour assister dans la destruction du noyau ou pour causer le chaos, elles peuvent être trouvées au refuge près des trains Cargos. **Les armes deviennent plus puissantes et diverses à mesure de votre promotion.** Plus d'informations à propos de ce que les outils peuvent faire peuvent être trouvées dans la partie **Rangs**.

Les armes données ne doivent pas être utilisées hors-service ou en service pour un autre groupe.

**Vous n'êtes pas autorisé à utiliser les outils de la PBST ou de la PET (Lance Incendie, Kit médical, ou bouclier de la PET) en service pour le TMS**

La zone d'équipement, ou le refuge du TMS, est montrée dans la photo ci-dessous.

![img](https://doy2mn9upadnk.cloudfront.net/uploads/default/original/4X/5/6/5/565a06af900dc2a6d47d19f38640bfda4d2cffcc.jpeg)

### Rencontrer la PBST
Pendant un raid, vous rencontrerez très probablement la sécurité du Computer Core. Ces membres vont activement vous attaquer.

Vous devez **tuer la sécurité à vue**, avec quelques exceptions.

* Vous ne pouvez pas tuer la PBST au spawn, ils sont protégés par les restrictions d'apparition.
* Vous ne pouvez pas attaquer un officier de la sécurité allant chercher son équipement, vous pouvez si il vous attaque en premier.
* Les membres de la PBST doivent à la fois être **en service pour la PBST** et **porter un uniforme officiel de la PBST** pour être attaqués, si le membre est un gradé **Tier 4 Défense Spéciale** ou supérieur dans la PBST, vous pouvez le tuer si il n'a pas d'uniforme. Vous pouvez ignorer cette règle si le membre de la PBST est une menace et/ou interfère avec votre objectif de fonte ou de gel du noyau.

Il y a une occasion rare où un officier de la PBST deviennent voyous et vont aider à détruire le noyau, ces gens apparaissent de temps en temps.

### Neutres
Ne sortez pas de votre chemin pour attaquer les neutres pendant un raid, s'ils ne sont pas une menace ou n'interfèrent pas avec votre objectif, merci de ne pas les tuer.

:::warning SVP, souvenez-vous 
En service en tant que membre du syndicat, vous êtes censé aider à causer le chaos en fondant ou gelant le noyau. Quelques fois, plusieurs membres du Syndicat essaye de réaliser des objectifs différents, la communication est la clé de la réussite! Travaillez ensemble pour atteindre un but commun et le succès suivra. 
:::

### Combinaisons de la PET/du noyau
Vous êtes autorisé à porter les combinaisons de la PET ou du noyau en service pour le TMS. Un uniforme n'est pas requis si vous portez ces combinaisons.

Les TMS sont autorisés à utiliser le casque d'astronaute pendant les raids, qui peut être utilisée quand la température est inférieure à 3000. ID du casque d'astronaute: *1081366*

### Événements
De temps en temps au Pinewood Computer Core, une catastrophe se produira. Ces catastrophes varient et peuvent changer dramatiquement le jeu. Ces catastrophes sont les suivantes :

* Fonte du noyau
* Gel du noyau
* Fuite de gaz
* Séisme
* Fuite de plasma
* Coupure électrique
* Fuite radiocative
* Extraterrestres

Vous n'êtes **pas** autorisé à régler la fuite radioactive sauf si elle empêche une fonte ou un gel du noyau.

### Agir avec les syndicalistes rebelles
De temps en temps, vous pouvez rencontrer un membre du syndicat essayant de sauver le noyau, enfreignant une règle du jeu, tuant ses équipiers ou abusant de ses armes. Ces membres sont considérés comme rebelles. Selon leurs actions, vous pourrez avoir à les tuer en utilisant la légitime défense s'ils vous attaquent selon des conditions non valides.

Si vous rencontrez un gradé (Soldat +) abusant leurs armes du TMS, envoyer un appel à la PIA pour Abus d'Arme Collective (Group Weapon Abuse) en notant les armes utilisés.

Les personnes autorisées à donner des ordres de KoS (Agent +) peuvent déclarer un individu comme rebelle.

Les syndicalistes rebelles sont:

* Tuer ses équipiers
* Tuer au spawn ou Tuer aléatoirement en masse
* Ne pas respecter le manuel à grande échelle

Les agents et supérieurs peuvent directement faire une déduction sur quelqu'un, mais vous pouvez aussi leur reporter ceux qui ne respectent pas ce manuel.

---
## Rangs
Les rangs peuvent donner à une personne de l'autorité ou des armes supplémentaires. Les soldats et les agents ont une limite de points, ce qui signifie que vous êtes remis à la quantité de points si vous dépassez cette limite après votre promotion. Ex : Promu en soldat à 70 points, redescendu à 60.

### Recrue
Recrue est le rang d'entrée dans le TMS. Il n'y a pas d'évaluation pour ce rang et pas de points requis.

#### Armes
* Pied de biche léger (Faibles dommages)

### Soldat
Soldat est le premier rang pouvant être acquis par une évaluation.

**Points requis**: 40 points   
**Limite**: 60 points

#### Armes
* Pied de biche
* Pistolet
* Fumigène

#### Évaluation
* Finir les Hautes Tours au PBSTAC en moins de 90 secondes
* Finir une arène de combat.
    * "Sword bot" de niveau 1 ou "Gun Bots" de niveau 1
* Répondre correctement à au moins 5 questions sur 8 posées
* Accord mineur de l'instructeur

### Agent
Agent est le premier rang autorisé à organiser des raids officiels dans le Syndicat.

Ce rang et les supérieurs nécessitent que vous soyez dans le serveur Discord du TMS. L'invitation est présente dans les liens sociaux du groupe TMS, vous devez avoir 13 ans pour le rejoindre.


**Points requis**: 120 points   
**Limite**: 140 points

#### Armes

* Pied de biche
* Pistolet
* Fumigène
* Fusil

#### Évaluation
* Répondre correctement à au moins 5 questions sur 6 posées.
    * Cela doit être réussi avant de passer à l'étage suivante.
* Organiser un raid observé par un instructeur
* Accord de l'instructeur

#### Autorité
* Donner des KoS officiels pour le TMS sur des membres d'un serveur
* Restreindre des salles au TMS uniquement
* Organiser des raids officiels (Niveau 0 et niveau 1)

### Capitaine
Capitaine est le troisième rang du Syndicat et est confié avec plus d'autorité.

**Points requis**: 200 points   
**Limite**: N/A</em>
#### Armes

* Pied de biche
* Pistolet
* Fumigène
* Fusil
* Pistolet-mitrailleur

#### Évaluation
* Finir "Bomb Barrage" de niveau 2
    * Cela doit être réussi avant de passer à l'étage suivante.
* Combattre 3 fois l'instructeur évaluateur
    * Deux de ces rounds sont des rounds d'épée ou des rounds d'armes.
    * Un fusil, un pistolet-mitrailleur et un pistolet sont donnés pour le(s) round(s) d'armes
    * Un Spec gamma est donné pour le(s) round(s) d'épée
    * Le troisième round est celui que vous n'aurez pas choisi. (ex : 2 armes & 1 épée, 2 épées & 1 armes) (pas de 3 roubds d'armes ou de 3 rounds d'épée)
* Accord majeur de l'instructeur
    * Si vous réussissez BB et terminez le combat mais n'avez pas l'accord, vous devez être invité par un Instructeur pour repasser l'évaluation.

#### Autorité
* Donner des KoS officiels pour le TMS sur des membres d'un serveur
* Restreindre des salles au TMS uniquement
* Organiser des raids officiels (Niveau 0, niveau 1 et niveau 2)
* **Ne pas tenir compte des ordres de KoS**
* **Ne pas tenir compte des restrictions de salle**

### Instructeur
Instructeur est le grade le plus élevé du syndicat. Ces membres sont les dirigeants du groupe : ils gèrent les opérations, les évaluations, les points et supervisent une majorité d'événement.

**Points requis**: N/A

#### Armes

* Pied de biche
* Pistolet amélioré
* Fusil amélioré
* Pistolet-mitrailleur amélioré
* Katana
* Fumigène puissant

#### Évaluation
Vous êtes choisi par les instructeurs actuels ou supérieurs et voté par tous les instructeurs. Vous pouvez être voté à n'importe quel rang.

#### Autorité
* Ces membres ont une entière autorité sur les membres moins gradés.
* Les instructeurs peuvent organiser des raids de niveau 0, de niveau 1, de niveau 2 et de niveau 3.
* Ils peuvent organiser des sessions d'entrainement de groupe officielles.

---

## Système de raid
Les raids officiels sont souvent organisés dans le Syndicat Mayhem. **Vous devez être un agent + pour organiser un raid du Syndicat officiel.**

Les points peuvent être gagnés pendant un raid officiel. Le montant donné varie en fonction du niveau de raid et de la durée de votre participation. Le comportement et la participation durant le raid sont pris en compte.

### Raids de niveau 0-3
**Raid de niveau 0** Les raids de niveau 0 ne donnent pas de points. Ils peuvent être organisés par un Agent +

**Raid de niveau 1** Les raids de niveau 1 donnent jusqu'à 3 points. Ils peuvent être organisés par un Agent +

**Raid de niveau 2** Les raids de niveau 2 donnent jusqu'à 5 points. Ils peuvent être organisés par un Capitaine +

**Raid de niveau 3** Les raids de niveau 3 donnent jusqu'à 7 points. Ils sont organisés par un Instructeur + et les capitaines peuvent héberger un serveur pendant ces MÉGA raids. Ces raids sont considérés comme des MÉGA raids et sont des raids sur plusieurs serveurs.

::: danger Soyez averti!
Les organisateurs du raid doivent suivre des règles à propos de l'organisation, ils le font tous. Les règles que ces membres doivent suivre sont indiquées dessous sous la section des Règles d'organisation des Raids 
:::

### Règles d'organisation
Tous les horaires de début ou de fin de journée sont décidés par l'UTC.

* Les organisateurs peuvent organiser jusqu'à **2** raids **CHAQUE** jour.
* Chaque organisateur peut seulement **1** raid à point **CHAQUE** jour.
* Si le dernier raid était un raid de **niveau 2**, vous devez attendre au moins 4 heures après le début de ce raid avant d'en organiser un autre.
* Si le dernier raid était un raid de **niveau 1**, vous devez attendre au moins 3 heures après le début de ce raid.
* Si le dernier raid était un raid de **niveau 0**, vous devez attendre au moins 2 heures après le début de ce raid avant d'en organiser un autre.
* Il doit avoir une période d'**UNE** heure entre la fin d'un raid officiel et le début du suivant.
* Les organisateurs ne peuvent pas commencer un raid 1 heure avant ou pendant le méga entrainement de la PBST.

Tout les organisateurs doivent les suivre. Les instructeurs ont l'autorité de terminer un raid plus tôt ou de l'annuler s'il viole les règles d'organisation. Il n'y a pas de temps d'attente pour les entrainements, mais les raids ne peuvent pas avoir lieu pendant une session d'entrainement.

---
### Types de Raids
Le Syndicat Mayhem organise une variété de raids différents, qui peuvent être vus en détail ci-dessous.

#### Fonte du noyau
Les raids de Fonte du noyau sont les raids les plus communs du TMS. Le but est simple, causer une fonte du noyau. Pour ce faire, il faut augmenter la température au-delà de 4000 degrés.

#### Gel du noyau
Les raids de Gel du noyau est un des raid les plus rares du TMS. Le but est simple, causer un gel du noyau. Pour ce faire, il faut diminuer la température en-dessous de -4000 degrés.

#### Chaos
Un des types de raid les plus amusants, le raid du Chaos ! Ce raid est quand le TMS brille dans ses phases les plus chaotiques. Pendant ce raid, des incendies majeurs sont causés, des trains nucléaires explosent et la température du noyau grimpe vers des nombres instables.

#### Post-raid
Ce n'est pas un objectif, mais c'en est un si le premier est atteint après qu'une fonte ou qu'un gel du noyau commence. Il s'agit de faire un Non aux survivants" ou un "Oui aux survivants" , ou rien du tout.

* **Oui aux survivants**

  Aider les survivants à s'échapper par les fusées

* **Non aux survivants**

  Ne laissez personne s'échapper avec les fusées

A la fin du raid, les membres du TMS sont amenés au Pinewood Builders Data Storage Facility, avec la commande `!pbdsf`. Afin de les regrouper, faire un registre des points pour tous les participants, puis congédier tout le monde.

---
### Vue d'ensemble du E-coolant
Enfin et surtout, après le début de la fonte du noyau, le E-coolant doit être pris en compte pour empêcher la PBST de sauver le noyau

Le E-coolant, ou liquide de refroidissement d'urgence, est un aspect du jeu qui laisse aux personnes une chance de sauver le noyau après le début de la fonte du noyau. Il est situé dans le secteur G, les deux portes nécessite le code 5334118 pour s'ouvrir.

Le E-coolant joue un rôle critique et doit être gardé sous contrôle, ou la température du noyau risque de redescendre à 3000 degrés.

Pour que l'E-coolant soit une réussite, les 3 réservoirs doivent avoir leur niveau entre 69 et 81 % pour être vert. Être au dessus ou en dessous de cet intervalle, rend le réservoir inutilisable.

Si les trois réservoirs sont dans le vert et que le minuteur atteint 0, il y a 50% de chance que le liquide de refroidissement fonctionne et que la température redescende à 3000 degrés.

![img](https://doy2mn9upadnk.cloudfront.net/uploads/default/original/4X/1/c/7/1c72246ac9ee343543c7209c30e401dee2f2fec0.jpeg)

---
## Entrainements
Les entrainements sont organisés périodiquement et sont réalisés dans n'importe quel complexe, même dans d'autres jeux. Les entrainements peuvent donner entre 1 et 4 points en fonction de la performance. Un mauvais comportement pendant les entrainements peut mener à un renvoi de l'entrainement, sans point accordé. Des points négatifs peuvent aussi est donnés si la situation le requiert.

---
## Les essentiels d'un Raid
Si vous ne pouvez accéder au salon de communication de raid, une autre façon pour communiquer par le chat de ROBLOX est d'utiliser les codes 10-. Ces codes vous permettent de transmettre des informations essentielles en seulement quelques chiffre, voici les plus communs :

`10-4` - Roger/Affirmatif    
`10-33` - Aide/Urgence - **Quand vous utilisez ce code, soyez sûr de spécifier votre localisation quand vous demander de l'aide**  
`10-22` - Annulation   
`10-17` - En route

Pendant un raid, il y a 3 zones clés, incluant :
* Noyau
* Refroidissement
* AC/Ventilateurs

Soyez sûrs d'être au moins un dans chacune de ces zones ou que vous circulez autour de ces zones pendant un raid.

Pendant un raid, l’endroit le plus important où vous pouvez être est le noyau, si vous remarquez qu'il y a beaucoup de raideurs où vous êtes, allez au noyau et aidez là-bas. Quand vous êtes à la salle de puissance du réacteur, vous pouvez coller le canon de votre arme cotre l'entrée du réacteur en vous tenant derrière le mur, ce qui vous autorise à tirer sur les gens arrivant sans qu'ils puissent vous toucher.

![img](https://iili.io/JLbPLu.png)

:::tip TIP
Si vous êtes tasé, vous pouvez facilement vous échapper en utilisant votre Jet-pack.   
![img](https://iili.io/JZPxBR.png)   
Cela fonctionnera uniquement si vous utilisez le Jet-pack ci-dessous   
:::


---

## Tactiques de Raid

### Tous les types de raids
À l'intérieur du QG de la PET, il y a un bloc de santé, en l'utilisant vous pouvez augmenter votre santé à 150 !

#### Noyau du réacteur
Dans la salle de puissance du réacteur, il y a plusieurs endroits où vous pouvez vous cacher pour attaquer. Par exemple
- Sur le gros fil rouge sur le toit
- Entre les contrôles du réacteur
- Sous la plateforme

Si vous cliquez sur les boutons de contrôle de la puissance assez rapidement, il se verrouille pour environ 30 secondes, empêchant la PBST de rechanger la puissance.

Sur les deux lasers latéraux, sur le bord si vous allez à l'intérieur, il y a un endroit où vous pouvez facilement vous cacher.

#### La salle de refroidissement
La salle de refroidissement a beaucoup d’endroits élevés vous permettant de tirer depuis le haut ou de garder facilement un œil.

Si l'unité centrale est gardé, vous pouvez utiliser l'entrée du noyau du réacteur ici, il y a à aussi une entrée dans la salle des serveurs, en revanche notez que cette entrée est la même que l'entrée de refroidissement. Cela sera donc utile uniquement si l'entrée ou le refroidissement est gardé.

#### Salle des ventilateurs
Dans la salle des ventilateurs, vous pouvez zoomer derrière la fenêtre pour activer les ventilateurs, pas besoin d'entrer dans la salle des ventilateurs !![Un GIF montrant comment faire ce qui préfère](/turnonfansfromoutside.gif)

### Raids du chaos
Dans un raid du chaos, si vous provoquez des incendies, vous pouvez porter l'uniforme Incendie de la PET pour vous protéger du feu. 
::: tip Info 
Selon la page du manuel, vous n'avez pas besoin de porter un uniforme du TMS au-dessus de celui-ci. 
:::

---
::: danger Merci de noter cela!
Ce manuel est toujours en cours de réalisation. 
:::

---



*Gloire au Syndicat !*

Signé, *Les Instructeurs et l'Architecte*

***Dernière mise à jour**: 26/5/2020*
